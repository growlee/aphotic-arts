import './stories.js'
import './animations.js'

document.addEventListener('DOMContentLoaded', () => {
    const $header = document.querySelector('header')
    const $footer = document.querySelectorAll('footer')
    
    window.addEventListener('scroll', () => {
        const { scrollY } = window
        if (scrollY > 10 && !$header.classList.contains('is-scrolled')) {
            $header.classList.add('is-scrolled')
        }
        else if (scrollY < 10 && $header.classList.contains('is-scrolled')) {
            $header.classList.remove('is-scrolled')
        }
    })


    const $scrollToLinks = [...document.querySelectorAll('[scroll-to]')]
    if ($scrollToLinks.length) {
        $scrollToLinks.forEach($trigger => {
            $trigger.addEventListener('click', (e) => {
                const target = $trigger.getAttribute('scroll-to')
                const offset = parseInt($trigger.getAttribute('scroll-to-offset') || 0)
                if (target) {
                    const $element = document.getElementById(target.replace(/^\#/, ''))
                    if ($element) {
                        window.scrollTo({
                            top: $element.offsetTop + offset,
                            left: 0,
                            behavior: 'smooth'
                        })
                    }
                }
                e.preventDefault()
            })
        })
    }
})