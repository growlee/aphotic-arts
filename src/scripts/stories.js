import Siema from 'siema'

document.addEventListener('DOMContentLoaded', () => {
    const $stories = [...document.querySelectorAll('.stories')]
    if ($stories.length) {
        $stories.forEach($story => {
            new Siema({
                selector: `#${$story.id}`,
                duration: 200,
                easing: 'ease-out',
                perPage: {
                    100: 1.25,
                    768: 3.50
                },
                startIndex: 0,
                draggable: true,
                multipleDrag: true,
                threshold: 32,
                loop: false,
                rtl: false,
                onInit: () => {},
                onChange: () => {}
            })
        })
    }
})