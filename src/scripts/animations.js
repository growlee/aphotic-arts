import lax from 'lax.js'

window.onload = function () {
    lax.init()
    lax.addDriver('scrollY', () => {
        const { scrollY } = window
        return scrollY
    })
    // lax.addElements('#news-and-updates', {
    //     scrollY: {
    //         translateY: [
    //             ['elInY', 'elCenterY', 'elOutY'],
    //             [0, -100, -200]
    //         ]
    //     }
    // })
    // lax.addElements('#games', {
    //     scrollY: {
    //         translateY: [
    //             ['elInY', 'elCenterY', 'elOutY'],
    //             [100, 0, -50]
    //         ]
    //     }
    // })
    // lax.addElements('#social-links', {
    //     scrollY: {
    //         translateY: [
    //             ['elInY', 'elCenterY', 'elOutY'],
    //             [50, 150, 300]
    //         ]
    //     }
    // })
    // lax.addElements('#contact-us', {
    //     scrollY: {
    //         translateY: [
    //             ['elInY', 'elCenterY', 'elOutY'],
    //             [50, 150, 300]
    //         ]
    //     }
    // })
}